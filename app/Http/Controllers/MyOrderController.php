<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Order;

class MyOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         
    

         // if (session()->has('user-email')) { 

                     if(session()->get('user-email')=='admin@co.com'){
                             $alldata=Product::paginate(4);
                               return view('total.my-order-admin',compact('alldata'));
                                                                      }
                   else {     
                            $alldata=Product::paginate(4);
                              return view('total.my-order-general',compact('alldata'));
                        } 
           //                                  }
           // else{
           //   return redirect('regis');
           //     }             


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $all_data=Order::paginate(4);
         return view('total.all-order-admin',compact('all_data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

           $input['profile_id']= session()->get('user-id');
           $input['product_id']= $id;
           Order::create($input);
           $message='Successfully Added This Product';
            return redirect('login/create')->with('message',$message);

            
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $message="your data has deleted !";
         $data=Order::findOrFail($id);
         $data->delete();
         return redirect()->back()->with('message',$message);
    }
}
