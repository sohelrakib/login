<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;

class MyPassword extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

         // if (session()->has('user-email')) { 

                     if(session()->get('user-email')=='admin@co.com'){
                            $data=Profile::findOrFail(session()->get('user-id'));
                            return view('total.change-password-admin',compact('data'));
                                                                      }
                   else {     
                           $data=Profile::findOrFail(session()->get('user-id'));
                           return view('total.change-password-general',compact('data'));
                        } 
           //                                  }
           // else{
           //   //echo "<h3> unauthorized accessed are not allowed </h3>";
           //   return redirect('regis');
           //     } 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
                  'new_pass1' => 'required|min:2',
                  'password' => 'required|min:2',
              ]);

       $input['old_pass']= md5($request->input('old_pass'));
       $input['new_pass1']= md5($request->input('new_pass1'));
       $input['password']= md5($request->input('password'));

        //$output=Profile::where('password', 'LIKE', $input['old_pass'])->get();
        $data=Profile::findOrFail($id);
   
       if($data['password']!==$input['old_pass'])
        {
            $message='wrong old password !!!';
            return redirect()->back()->with('message',$message); 
        }
      

       else if($input['new_pass1']!==$input['password'])
       {
        $message="new password did not match!!";
        return redirect()->back()->with('message',$message); 
       }

       else 
       {
        
        // return $input=$request->all();
         $inputs['password']= $input['password'];
         $data=Profile::findOrFail($id);
         $data->update($inputs);

         $message="new password updated successfully!!";
         return redirect()->back()->with('message',$message); 
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
