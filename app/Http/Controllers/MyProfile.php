<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;

class MyProfile extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       

         // if (session()->has('user-email')) { 

                     if(session()->get('user-email')=='admin@co.com'){
                           $all_data=Profile::where('id', 'LIKE', session()->get('user-id'))->get();
                           return view('total.my-profile-admin',compact('all_data'));
  
                                                                      }
                     else {  
                           $all_data=Profile::where('id', 'LIKE', session()->get('user-id'))->get(); 
                          return view('total.my-profile-general',compact('all_data'));

                         } 
           //                                 }
           // else{
           //   //echo "<h3> unauthorized accessed are not allowed </h3>";
           //   return redirect('regis');
           //   }      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      

         // if (session()->has('user-email')) 
         // {  



                 if(session()->get('user-email')=='admin@co.com'){
                              $data=Profile::findOrFail($id);
                              return view('total.edit-profile-admin',compact('data'));
                                                                  }
                  else{
                              $data=Profile::findOrFail($id);
                              return view('total.edit-profile-general',compact('data'));
                  }                                                



          // }
          //  else{
          //    //echo "<h3> unauthorized access are not allowed </h3>";
          //     return redirect('regis');
          //  }  

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
                  'name' => 'required|min:2',
              ]);

         $data=profile::findOrFail($id);

        $input['name']=$request->input('name');




        
           $filename=$data['file_name'];
           $filesize=$data['file_size'];

        if($request->hasFile('image'))
        {
          $str = 'abcedvwxyz';
          $shuffled = str_shuffle($str);

          $filename=$request->image->getClientOriginalName();
          $filename=$shuffled.$filename;
          $filesize=$request->image->getClientSize();
          $request->image->storeAs('public/upload',$filename);
        }

           $input['file_name']=$filename;
           $input['file_size']=$filesize;





              $message="successfully data updated !!!";
              
             
              //return $data['file_name'];
              $data->update($input);
              return redirect('mypro')->with('message',$message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
