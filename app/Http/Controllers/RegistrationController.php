<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;

class RegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('total.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('total.registration');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
               'name' => 'required|min:2',
               'email'=>'required|email|unique:profile,email',
               'password' => 'required|min:2',
               'password2' => 'required|min:2',
           ]);

             $input['name']= $request->input('name');
             $input['email']= $request->input('email');
             $input['password']= md5($request->input('password'));
             $input['password2']= md5($request->input('password2'));


                $filename='';
                $filesize=0;

             if($request->hasFile('image'))
             {
               $str = 'abcedvwxyz';
               $shuffled = str_shuffle($str);

               $filename=$request->image->getClientOriginalName();
               $filename=$shuffled.$filename;
               $filesize=$request->image->getClientSize();
               $request->image->storeAs('public/upload',$filename);
             }
            

                $input['file_name']=$filename;
                $input['file_size']=$filesize;


         if( $input['password']== $input['password2']){
             Profile::create($input);
             $message='you registered successfully! Login now!!';
            return redirect('regis')->with('message',$message);
                                                       }
          else{
            $message="your password did not match";
            return redirect()->back()->with('message',$message);
            
          }                                             
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
