<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Order;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
             $name=$request->input('name');

             if(!empty($name))
             {
              $alldata=product::where('product_name','LIKE','%'.$name.'%')->paginate(4);
             }              
             else{
              $alldata=product::paginate(4);
             }

       // if (session()->has('user-email')) { 

                   if(session()->get('user-email')=='admin@co.com'){
                           
                            return view('total.product-all-admin',compact('alldata'));
                                                                    }
                 else {     
                           
                           return view('total.product-all-general',compact('alldata'));
                      } 
        //               }
        // else{
        //     return redirect('regis');
        // }                                                

           // $alldata=Product::all();
        // $alldata=$alldata->paginate(4);
        // return view('total.product-all',compact('alldata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
         // if (session()->has('user-email')) {
                     return view('total.product-add');
          //                                    }
          // else{
          //   //echo "<h3> unauthorized accessed are not allowed </h3>";
          //    return redirect('regis');
          // }                                   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
               'product_name' => 'required|min:2|unique:product,product_name',
               'price'=>'required',
           ]);

        $input=$request->all();



            $filename='';
            $filesize=0;

         if($request->hasFile('image'))
         {
           $str = 'abcedvwxyz';
           $shuffled = str_shuffle($str);

           $filename=$request->image->getClientOriginalName();
           $filename=$shuffled.$filename;
           $filesize=$request->image->getClientSize();
           $request->image->storeAs('public/products',$filename);
         }
         

            $input['file_name']=$filename;
            $input['file_size']=$filesize;

    

        Product::create($input);
        $message=' product created successfully!! ';
        return redirect('product')->with('message',$message);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        // if (session()->has('user-email')) 
        // {  



                if(session()->get('user-email')=='admin@co.com'){
                             $data=Product::findOrFail($id);
                             return view('total.product-all-edit',compact('data'));
                                                                 }                                              
         // }
         //  else{
         //    return redirect('regis');
         //  } 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
               'product_name' => "required|min:2|unique:product,product_name,$id",
               'price'=>'required',
           ]);
               
               $data=product::findOrFail($id);
               $input['product_name']=$request->input('product_name');
               $input['price']=$request->input('price');

                  $filename=$data['file_name'];
                  $filesize=$data['file_size'];

               if($request->hasFile('image'))
               {
                 $str = 'abcedvwxyz';
                 $shuffled = str_shuffle($str);

                 $filename=$request->image->getClientOriginalName();
                 $filename=$shuffled.$filename;
                 $filesize=$request->image->getClientSize();
                 $request->image->storeAs('public/products',$filename);
               }

                  $input['file_name']=$filename;
                  $input['file_size']=$filesize;


  
              $message="successfully data updated !!!";
             
              $data->update($input);
              return redirect()->back()->with('message',$message);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        

          $message="your data has deleted !";
          $data=Product::findOrFail($id);
          $data->delete();
          return redirect()->back()->with('message',$message);

       // // $data=Order::where('product_id','=',$id);             
       //  $data = Order::findOrFail($id);
       //  return $data;
    }
}
