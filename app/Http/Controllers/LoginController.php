<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use App\Order;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        // $this->middleware('auth', ['only' => ['create', 'store', 'edit', 'delete']]);
        // // Alternativly
        $this->middleware('checkLogout', ['except' => ['store']]);
    }

    public function index(Request $request)
    {
       // if (session()->has('user-email')) 
       // {  

              $name=$request->input('name');
               if(!empty($name)){
                  $alldata=Profile::where('id','LIKE',$name)->paginate(4);
                                }
              else              { 
                 $alldata=Profile::paginate(4);
                                }



               if(session()->get('user-email')=='admin@co.com'){
                         

                 return view('total.member-admin',compact('alldata'));

                                                                }
                else{
                   
                    return view('total.member-general',compact('alldata'));
                }                                                



        // }
        //  else{
        //     return redirect('regis');
        //  }                                 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       // if (session()->has('user-email')) {  

                                   if(session()->get('user-email')=='admin@co.com')
                                    {
                                               $all_data=Order::where('profile_id', 'LIKE', session()->get('user-id'))->get();
                                               return view('total.admin',compact('all_data'));

                                    }   
                                    else
                                    {
                                              $all_data=Order::where('profile_id', 'LIKE', session()->get('user-id'))->get();
                                               return view('total.general',compact('all_data'));
                                    }  
                      //  }
                      // else{
                      //  // echo "<h3> unauthorized access are not allowed </h3>";
                      //    return redirect('regis');
                      // }       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
         $input['email']= $request->input('email');
         $input['password']=md5($request->input('password'));

        $output=Profile::where('email', 'LIKE', $input['email'])->get();

        if(count($output)==0)
        {
            $message='wrong email address !!!';
            return redirect('regis')->with('message',$message);
        }
          
          $checkpass='';
          $user_id=0;
        foreach($output as $record){
              $user_id=$record->id;
              $password = $record->password;
               $checkpass=$password;

            }



           if($input['password']===$checkpass)
            {
                    session()->put('user-email', $input['email']);
                    session()->put('user-id', $user_id);
                  
                    // if (session()->has('user-email')) {
                   
                      
                                   if($input['email']=='admin@co.com')
                                    {
                                               $all_data=Order::where('profile_id', 'LIKE', session()->get('user-id'))->get();
                                               return view('total.admin',compact('all_data'));

                                    }   
                                    else
                                    {   
                                                $all_data=Order::where('profile_id', 'LIKE', session()->get('user-id'))->get();
                                               return view('total.general',compact('all_data'));
                                    }  
                      //                                          }
                      // else{
                      //   //echo "unauthorized access are not allowed";
                      //    return redirect('regis');
                      // }                                         
                                              
            }
            else 
             {
                    $message='wrong password ! try again.';
                    return redirect('regis')->with('message',$message);
             }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

          $message="The Member Has Deleted";
          $data=Profile::findOrFail($id);
          $data->delete();
          return redirect()->back()->with('message',$message);
    }
}
