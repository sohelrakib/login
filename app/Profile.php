<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table="profile";

    protected $fillable=[
         'name',
         'email',
         'password',
         'file_name',
         'file_size'
    ];
}
