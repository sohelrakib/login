<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
     protected $table="order";

    protected $fillable=[
         'profile_id',
         'product_id'
    ];


    public function getProductName()
    {
    	return Product::where('id',$this->product_id)->first()->product_name;
    }


    public function getProductPrice()
    {
    	return Product::where('id',$this->product_id)->first()->price;
    }

    public function getProductFileName()
    {
        return Product::where('id',$this->product_id)->first()->file_name;
    }

    public function getProfileName()
    {
        return Profile::where('id',$this->profile_id)->first()->name;
    }
}
