@extends('master')


    @section('title')
      welcome for registration here
    @endsection



    @section('content')  
 	  	         <h2 class="first-heading text-center"> Welcome to Registration Here:: </h2>

          <p class="messsage text-center"> {{ session('message') }} </p>

                    

                    @if ($errors->any())
                         <div class="alert alert-danger">
                             <ul>
                                 @foreach ($errors->all() as $error)
                                     <li>{{ $error }}</li>
                                 @endforeach
                             </ul>
                         </div>
                     @endif



 	  	        
 	  	         {!! Form::open(array('route'=>'regis.store','files' => true)) !!}
                        <div class="form-group row">
                               <div class="col-md-3 col-sm-3 col-xs-3">
                                   <label class="contrl-label" for="name">Name:</label>
                               </div>
                               <div class="col-md-9 col-sm-9 col-xs-9">
                                    <input type="text" class="form-control" name="name"  value="{{ old('name') }}" required>
                                </div>
                         </div>


                         <div class="form-group row">
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                     <label class="control-label" for="email">Email: </label>
                                 </div>
                                 <div class="col-md-9 col-sm-9 col-xs-9">
                                      <input type="email" class="form-control" name="email"  value="{{ old('email') }}" required> 
                                 </div>
                         </div>


                         <div class="form-group row">
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                     <label class="control-label" for="password">Password: </label>
                                 </div>
                                 <div class="col-md-9 col-sm-9 col-xs-9">
                                      <input type="password" class="form-control" name="password"  value="{{ old('password') }}" required> 
                                 </div>
                         </div>


                          <div class="form-group row">
                                 <div class="col-md-3 col-sm-3 col-xs-3">
                                      <label class="control-label" for="password2">Password(again): </label>
                                  </div>
                                  <div class="col-md-9 col-sm-9 col-xs-9">
                                       <input type="password" class="form-control" name="password2"  value="{{ old('password2') }}" required> 
                                  </div>
                          </div>


                           <div class="form-group row">
                                 <div class="col-md-3 col-sm-3 col-xs-3">
                                      <label class="control-label" for="image">Image: </label>
                                  </div>
                                  <div class="col-md-9 col-sm-9 col-xs-9">
                                       <input type="file" class="form-control" name="image" id="image"> 
                                  </div>
                           </div>
 
                         <input class="btn btn-success" type="submit" value="Submit">
 	  	          {!! Form::close() !!} 


       

    @endsection

