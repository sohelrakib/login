@extends('master')


    @section('title')
      ***admin page
    @endsection



    @section('content')  
                 <h2 class="first-heading text-center">**** Admin Page * </h2>

                   <div class="row">
                              <div class="col-md-8 col-sm-8">

          <p class="messsage"> {{ session('message') }} </p>

                     <h4><b> All Order : </b></h4> <hr>
                
                                            <div class="table-responsive">          
                                               <table class="table">
                                                  <thead>
                                       <tr>
                                        <th> order by </th> 
                                        <th> name </th>
                                        <th> price </th>
                                       </tr>
                                                    </thead>

                                                    <tbody>
                                                         
                                     @foreach($all_data as $data)
                                        <tr>
                                           <td> {{$data->getProfileName()}} </td>
                                           <td> {{$data->getProductName()}} </td>
                                           <td> {{$data->getProductPrice()}} </td>
                                            
                                        </tr>
                                   
                                     @endforeach 
                                                    </tbody>
                                                </table>
                                            </div> 
                                   {!!$all_data->render() !!} 

                              </div>
                              <div class="col-md-4 col-sm-4 sidebar ">
                               <nav class="navbar navbar-inverse ">
                                <div class="navbar-header">
                                  <ul>
                                    <li>
                                 <button type="button" class="btn btn-success navbar-toggle sidebar-btn" data-toggle="collapse" data-target="#myNavbar2">
                                      SIDEBAR
                                          
                                   </button>
                                 </li>
                               </ul>
                                </div>

                                <div class="collapse navbar-collapse" id="myNavbar2">
                                 <ul class="sidebar-ul">
                                    <li> <a href="{{route('login.create')}}"><button type="button" class="btn btn-primary"> Home </button> </a> </li>

                                    <li> <a href="{{route('product.create')}}"><button type="button" class="btn btn-primary"> Add New Product </button> </a> </li> 

                                     <li> <a href="{{route('product.index')}}"><button type="button" class="btn btn-primary">  All Product </button> </a> </li>

                                     <li> <a href="{{route('login.index')}}"><button type="button" class="btn btn-primary"> Our Member </button></a> </li> 

                                     <li> <a href="{{route('order.index')}}"><button type="button" class="btn btn-primary"> Order a Product </button></a> </li>

                                     <li> <a href="{{route('order.create')}}"><button type="button" class="btn btn-primary"> All Order </button></a> </li>

                                     <li> <a href="{{route('logout.index')}}"><button type="button" class="btn btn-primary"> Logout </button></a> </li>

                                      <li> <a href="{{route('mypro.index')}}"><button type="button" class="btn btn-primary"> My Profile </button></a> </li>

                                      <li> <a href="{{route('mypass.index')}}"><button type="button" class="btn btn-primary"> Change Password </button></a> </li>

                                      
                                   
                                 </ul>  
                                 </div>  
                                 </nav> 
                              </div>           <!-- col-md-4 col-sm-4 -->
                   </div>

    @endsection

