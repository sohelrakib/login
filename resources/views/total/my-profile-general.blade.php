@extends('master')


    @section('title')
      ***profile
    @endsection



    @section('content')  
                 <h2 class="first-heading text-center">**** General Page * </h2>

                   <div class="row">
                              <div class="col-md-8 col-sm-8">
                      
              <p class="messsage" style="color:green"> {{ session('message') }} </p>                     
                                @foreach($all_data as $data)            
                                  <div class="row">
                                     <div class="col-sm-6">
        <img src="{{ asset('storage/upload/'.$data->file_name)}}" alt="user profile" class="pro-img img-responsive">
                                     </div>
                                     <div class="col-sm-6">
                                         <div class="table-responsive">          
                                            <table class="table tab-padding">
                                                
                                             
                                           <tr>
                                             <th>  NAME    </th> 
                                             <td > {{$data->name}} </td> 
                                           </tr>
                                           <tr>
                                             <th>  EMAIL    </th> 
                                             <td> {{$data->email}} </td> 
                                           </tr>
                                           <tr>
                                             <th>  ACTION     </th> 
                                             <td> <button type="button" class="btn btn-primary"><a href="{{route('mypro.edit',$data->id)}}"> click for edit</a></button></td> 
                                           </tr>
                                                </table>
                                                       
                                          </div>  
                                     </div>
                                  </div> 
                                  @endforeach    
                                                     


                              </div>

                            <div class="col-md-4 col-sm-4 sidebar ">
                                                           <nav class="navbar navbar-inverse ">
                                                            <div class="navbar-header">
                                                              <ul>
                                                                <li>
                                                             <button type="button" class="btn btn-success navbar-toggle sidebar-btn" data-toggle="collapse" data-target="#myNavbar2">
                                                                  SIDEBAR
                                                                      
                                                               </button>
                                                             </li>
                                                           </ul>
                                                            </div>

                                                            <div class="collapse navbar-collapse" id="myNavbar2">
                               <ul class="sidebar-ul">
                                  <li> <a href="{{route('login.create')}}"><button type="button" class="btn btn-primary"> Home </button></a> </li>

                                  <!-- <li> <button type="button" class="btn btn-primary"><a href="{{route('product.create')}}"> Add New Product</a></button> </li> --> 

                                   <li> <a href="{{route('product.index')}}"><button type="button" class="btn btn-primary"> All Product </button></a> </li>

                                   <li> <a href="{{route('login.index')}}"><button type="button" class="btn btn-primary"> Our Member </button></a> </li> 

                                   <li> <a href="{{route('order.index')}}"><button type="button" class="btn btn-primary"> Order a Product </button></a> </li>

                                   <li> <a href="{{route('logout.index')}}"><button type="button" class="btn btn-primary"> Logout </button></a> </li>

                                    <li> <a href="{{route('mypro.index')}}"><button type="button" class="btn btn-primary">  My Profile </button></a> </li>

                                    <li> <a href="{{route('mypass.index')}}"><button type="button" class="btn btn-primary"> Change Password </button></a> </li>

                                    
                                 
                               </ul>     
                            </div>  
                            </nav> 
                        </div>           <!-- col-md-4 col-sm-4 -->
                   </div>

    @endsection

