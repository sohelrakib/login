@extends('master')


    @section('title')
      add new product
    @endsection




    @section('content')  
 	  	    <h2 class="first-heading text-center">**** Admin Page * </h2>

	           <div class="row">
		                  <div class="col-md-8 col-sm-8">
                        <h4><b> Edit This Product: </b></h4><br>
                                      @if ($errors->any())
                                           <div class="alert alert-danger">
                                               <ul>
                                                   @foreach ($errors->all() as $error)
                                                       <li>{{ $error }}</li>
                                                   @endforeach
                                               </ul>
                                           </div>
                                       @endif


                                      {!! Form::open(['route' => ['product.update', $data->id],'method' => 'put','files' => true]) !!}
		     
                                              <div class="form-group row">
                                                   
                                                     
                                                     <div class="col-md-3 col-sm-3 col-xs-3">
                                                         <label class="contrl-label" for="product_name">Product Name:</label>
                                                     </div>
                                                     <div class="col-md-9 col-sm-9 col-xs-9">
                                                          <input type="text" class="form-control" name="product_name"  value="{{ ($data->product_name) }}" required>
                                                      </div>
                                               </div>


                                               <div class="form-group row">
                                                      <div class="col-md-3 col-sm-3 col-xs-3">
                                                           <label class="control-label" for="price">Price: </label>
                                                       </div>
                                                       <div class="col-md-9 col-sm-9 col-xs-9">
                                                            <input type="number" class="form-control" name="price"  value="{{ ($data->price) }}" required> 
                                                       </div>
                                               </div>


                                               <div class="form-group row">
                                                     <div class="col-md-3 col-sm-3 col-xs-3">
                                                          <label class="control-label" for="image">Image: </label>
                                                      </div>
                                                      <div class="col-md-9 col-sm-9 col-xs-9">
                                                           <input type="file" class="form-control" name="image" id="image"> 
                                                      </div>
                                                </div>


                                               <input class="btn btn-success" type="submit" value="Submit">

                       	  	          {!! Form::close() !!} 

                       	  	          <p class="messsage text-center"> {{ session('message') }} </p>
		                  </div>

	                     <div class="col-md-4 col-sm-4 sidebar ">
                        <nav class="navbar navbar-inverse ">
                         <div class="navbar-header">
                           <ul>
                             <li>
                          <button type="button" class="btn btn-success navbar-toggle sidebar-btn" data-toggle="collapse" data-target="#myNavbar2">
                               SIDEBAR
                                   
                            </button>
                          </li>
                        </ul>
                         </div>

                         <div class="collapse navbar-collapse" id="myNavbar2">
                          <ul class="sidebar-ul">
                             <li> <a href="{{route('login.create')}}"><button type="button" class="btn btn-primary"> Home </button> </a> </li>

                             <li> <a href="{{route('product.create')}}"><button type="button" class="btn btn-primary"> Add New Product </button> </a> </li> 

                              <li> <a href="{{route('product.index')}}"><button type="button" class="btn btn-primary">  All Product </button> </a> </li>

                              <li> <a href="{{route('login.index')}}"><button type="button" class="btn btn-primary"> Our Member </button></a> </li> 

                              <li> <a href="{{route('order.index')}}"><button type="button" class="btn btn-primary"> Order a Product </button></a> </li>

                              <li> <a href="{{route('order.create')}}"><button type="button" class="btn btn-primary"> All Order </button></a> </li>

                              <li> <a href="{{route('logout.index')}}"><button type="button" class="btn btn-primary"> Logout </button></a> </li>

                               <li> <a href="{{route('mypro.index')}}"><button type="button" class="btn btn-primary"> My Profile </button></a> </li>

                               <li> <a href="{{route('mypass.index')}}"><button type="button" class="btn btn-primary"> Change Password </button></a> </li>

                               
                            
                          </ul>  
                          </div>  
                          </nav> 
                       </div>           <!-- col-md-4 col-sm-4 -->
	           </div>

    @endsection



