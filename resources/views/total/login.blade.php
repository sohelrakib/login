@extends('master')


    @section('title')
      login to your account
    @endsection



    @section('content')  
 	  	         <h2 class="first-heading text-center"> Welcome to Login in Your Account </h2>




 	  	                            

 	  	                            @if ($errors->any())
 	  	                                 <div class="alert alert-danger">
 	  	                                     <ul>
 	  	                                         @foreach ($errors->all() as $error)
 	  	                                             <li>{{ $error }}</li>
 	  	                                         @endforeach
 	  	                                     </ul>
 	  	                                 </div>
 	  	                             @endif


                 <p class="messsage text-center" style="color:blue"> {{ session('message') }} </p>
 	  	         	  	        
 	  	         	  	         {!! Form::open(array('route'=>'login.store')) !!}

 	  	                                 <div class="form-group row">
 	  	                                        <div class="col-md-3 col-sm-3 col-xs-3">
 	  	                                             <label class="control-label" for="email">Email: </label>
 	  	                                         </div>
 	  	                                         <div class="col-md-9 col-sm-9 col-xs-9">
 	  	                                              <input type="email" class="form-control" name="email"  value="{{ old('email') }}" required> 
 	  	                                         </div>
 	  	                                 </div>


 	  	                                 <div class="form-group row">
 	  	                                        <div class="col-md-3 col-sm-3 col-xs-3">
 	  	                                             <label class="control-label" for="password">Password: </label>
 	  	                                         </div>
 	  	                                         <div class="col-md-9 col-sm-9 col-xs-9">
 	  	                                              <input type="password" class="form-control" name="password"  value="{{ old('password') }}" required> 
 	  	                                         </div>
 	  	                                 </div>

 	  	                                 <input class="btn btn-success" type="submit" value="Submit">
 	  	         	  	          {!! Form::close() !!} 


 	  	               
            
 	  	         

 	  	               
    @endsection