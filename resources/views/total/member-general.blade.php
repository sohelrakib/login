@extends('master')


    @section('title')
      admin page
    @endsection



    @section('content')  
 	  	         <h2 class="first-heading text-center"> **** General Page * </h2>

	               <div class="row">
			                  <div class="col-md-8 col-sm-8">
                            <div class="search">

                          {!! Form::open(['route'=>'login.index','method'=>'GET','class'=>'search-form']) !!}
                          <div class="form-group row">
                               <div class="col-md-8 col-sm-8 col-xs-8">

                                     <select name="name"  class="form-control">
                                          <option value=""></option>
                                        @foreach($alldata as $data)  
                                          <option value="{{$data->id}}">{{$data->name}}</option>
                                        @endforeach
                                    </select>
                               </div>
                               <div class="col-md-4 col-sm-4 col-xs-4">
                   {!!Form::submit('search',array('class'=>'search-button btn btn-default')) !!}
                               </div>
                          </div>
                        {!! Form:: close() !!}


                             </div>  <!--  search -->
			                       
                               <h3 class="text-center"> All Member </h3>
                                       <div class="table-responsive">          
                                              <table class="table">
                                                    <thead>
                                                         <tr>
                                                            <th> NAME </th> 
                                                            <th> EMAIL </th>
                                                            <th> IMAGE </th>
                                                         </tr>
                                                   </thead>

                                                   <tbody>
                                                         
                                                 @foreach($alldata as $data)
                                                          <tr>
                                                             <td> {{$data->name}} </td>
                                                             <td> {{$data->email}} </td>
                                                             <td>
                                                 <img src="{{ asset('storage/upload/'.$data->file_name)}}" alt="user profile" class="member-img img-responsive">
                                                             </td>
                                                          </tr>
                                                
                                                 @endforeach 
                                                   </tbody>
                                               </table>
                                       </div>   
                                               {!!$alldata->render() !!}                



			                  </div>

	                     <div class="col-md-4 col-sm-4 sidebar ">
                                                      <nav class="navbar navbar-inverse ">
                                                       <div class="navbar-header">
                                                         <ul>
                                                           <li>
                                                        <button type="button" class="btn btn-success navbar-toggle sidebar-btn" data-toggle="collapse" data-target="#myNavbar2">
                                                             SIDEBAR
                                                                 
                                                          </button>
                                                        </li>
                                                      </ul>
                                                       </div>

                                                       <div class="collapse navbar-collapse" id="myNavbar2">
                          <ul class="sidebar-ul">
                             <li> <a href="{{route('login.create')}}"><button type="button" class="btn btn-primary"> Home </button></a> </li>

                             <!-- <li> <button type="button" class="btn btn-primary"><a href="{{route('product.create')}}"> Add New Product</a></button> </li> --> 

                              <li> <a href="{{route('product.index')}}"><button type="button" class="btn btn-primary"> All Product </button></a> </li>

                              <li> <a href="{{route('login.index')}}"><button type="button" class="btn btn-primary"> Our Member </button></a> </li> 

                              <li> <a href="{{route('order.index')}}"><button type="button" class="btn btn-primary"> Order a Product </button></a> </li>

                              <li> <a href="{{route('logout.index')}}"><button type="button" class="btn btn-primary"> Logout </button></a> </li>

                               <li> <a href="{{route('mypro.index')}}"><button type="button" class="btn btn-primary">  My Profile </button></a> </li>

                               <li> <a href="{{route('mypass.index')}}"><button type="button" class="btn btn-primary"> Change Password </button></a> </li>

                               
                            
                          </ul>     
                       </div>  
                       </nav> 
                   </div>           <!-- col-md-4 col-sm-4 -->
	               </div>

    @endsection

