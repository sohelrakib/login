@extends('master')


    @section('title')
      ***admin page
    @endsection



    @section('content')  
                 <h2 class="first-heading text-center">**** Admin Page * </h2>

                   <div class="row">
                              <div class="col-md-8 col-sm-8">

          <p class="messsage" style="color:green"> {{ session('message') }} </p>

                     <h3> my order </h3>
                
                                            <div class="table-responsive">          
                                               <table class="table">
                                                  <thead>
                                       <tr>
                                        <th> image </th> 
                                        <th> name </th>
                                        <th> price </th>
                                        <th> remove </th>
                                       </tr>
                                                    </thead>

                                                    <tbody>
                            <?php $total_price=0; ?>                             
                                     @foreach($all_data as $data)
                                        <tr>
                                           <td> <img src="{{ asset('storage/products/'.$data->getProductFileName())}}" alt="product" class="product-img img-responsive"> </td>
                                           <td> {{$data->getProductName()}} </td>
                                           <td> {{$data->getProductPrice()}} </td>
                                            <td>  

            {!! Form::open(['route' => ['order.destroy', $data->id],'method' => 'delete']) !!}
            {!!Form::hidden('id',$data->id)!!}
            {!!Form::submit('click for delete',array('class'=>'btn btn-danger','onclick'=>'return myFunction()'))!!}
            {!! Form::close() !!}                            

                                            </td>
                                        </tr>
                                    <?php $total_price+=$data->getProductPrice(); ?>
                                     @endforeach 
                                                    </tbody>
                                                </table>
                                            </div> 
       <p class="text-center total-cost"> total cost :  {{$total_price}} </p>                            

                              </div>
                              <div class="col-md-4 col-sm-4 sidebar ">
                               <nav class="navbar navbar-inverse ">
                                <div class="navbar-header">
                                  <ul>
                                    <li>
                                 <button type="button" class="btn btn-success navbar-toggle sidebar-btn" data-toggle="collapse" data-target="#myNavbar2">
                                      SIDEBAR
                                          
                                   </button>
                                 </li>
                               </ul>
                                </div>

                                <div class="collapse navbar-collapse" id="myNavbar2">
                                 <ul class="sidebar-ul">
                                    <li> <a href="{{route('login.create')}}"><button type="button" class="btn btn-primary"> Home </button> </a> </li>

                                    <li> <a href="{{route('product.create')}}"><button type="button" class="btn btn-primary"> Add New Product </button> </a> </li> 

                                     <li> <a href="{{route('product.index')}}"><button type="button" class="btn btn-primary">  All Product </button> </a> </li>

                                     <li> <a href="{{route('login.index')}}"><button type="button" class="btn btn-primary"> Our Member </button></a> </li> 

                                     <li> <a href="{{route('order.index')}}"><button type="button" class="btn btn-primary"> Order a Product </button></a> </li>

                                     <li> <a href="{{route('order.create')}}"><button type="button" class="btn btn-primary"> All Order </button></a> </li>

                                     <li> <a href="{{route('logout.index')}}"><button type="button" class="btn btn-primary"> Logout </button></a> </li>

                                      <li> <a href="{{route('mypro.index')}}"><button type="button" class="btn btn-primary"> My Profile </button></a> </li>

                                      <li> <a href="{{route('mypass.index')}}"><button type="button" class="btn btn-primary"> Change Password </button></a> </li>

                                      
                                   
                                 </ul>  
                                 </div>  
                                 </nav> 
                              </div>           <!-- col-md-4 col-sm-4 -->
                   </div>

    @endsection

