@extends('master')


    @section('title')
      change ur password
    @endsection



    @section('content')  
                 <h2 class="first-heading text-center">**** General Page * </h2>

                   <div class="row">
                              <div class="col-md-8 col-sm-8">
                                  
                                 <h1 class="text-center"> change your password </h1>     
                                       
                                              @if ($errors->any())
                                                   <div class="alert alert-danger">
                                                       <ul>
                                                           @foreach ($errors->all() as $error)
                                                               <li>{{ $error }}</li>
                                                           @endforeach
                                                       </ul>
                                                    </div>
                                               @endif


                  <p class="messsage text-center" style="color:red"> {{ session('message') }} </p>        


                    {!! Form::open(['route' => ['mypass.update', $data->id],'method' => 'put']) !!}
                                 <div class="form-group row">
                                         <div class="col-md-3 col-sm-3 col-xs-3">
                                             <label class="contrl-label" for="old_pass"> Old Password:</label>
                                         </div>
                                         <div class="col-md-9 col-sm-9 col-xs-9">
                                             <input type="password" class="form-control" name="old_pass" required>
                                         </div>
                                 </div>

                                  <div class="form-group row">
                                         <div class="col-md-3 col-sm-3 col-xs-3">
                                             <label class="control-label" for="new_pass1">New Password: </label>
                                         </div>
                                         <div class="col-md-9 col-sm-9 col-xs-9">
                                              <input type="password" class="form-control" name="new_pass1" required> 
                                         </div>
                                  </div>

                                  <div class="form-group row">
                                         <div class="col-md-3 col-sm-3 col-xs-3">
                                             <label class="control-label" for="password">Confirm Password: </label>
                                         </div>
                                         <div class="col-md-9 col-sm-9 col-xs-9">
                                              <input type="password" class="form-control" name="password" required> 
                                         </div>
                                  </div>

                                  <input class="btn btn-success" type="submit" value="Submit">
                         {!! Form::close() !!}                                   
                           





                              </div>
                          <div class="col-md-4 col-sm-4 sidebar ">
                                                         <nav class="navbar navbar-inverse ">
                                                          <div class="navbar-header">
                                                            <ul>
                                                              <li>
                                                           <button type="button" class="btn btn-success navbar-toggle sidebar-btn" data-toggle="collapse" data-target="#myNavbar2">
                                                                SIDEBAR
                                                                    
                                                             </button>
                                                           </li>
                                                         </ul>
                                                          </div>

                                                          <div class="collapse navbar-collapse" id="myNavbar2">
                             <ul class="sidebar-ul">
                                <li> <a href="{{route('login.create')}}"><button type="button" class="btn btn-primary"> Home </button></a> </li>

                                <!-- <li> <button type="button" class="btn btn-primary"><a href="{{route('product.create')}}"> Add New Product</a></button> </li> --> 

                                 <li> <a href="{{route('product.index')}}"><button type="button" class="btn btn-primary"> All Product </button></a> </li>

                                 <li> <a href="{{route('login.index')}}"><button type="button" class="btn btn-primary"> Our Member </button></a> </li> 

                                 <li> <a href="{{route('order.index')}}"><button type="button" class="btn btn-primary"> Order a Product </button></a> </li>

                                 <li> <a href="{{route('logout.index')}}"><button type="button" class="btn btn-primary"> Logout </button></a> </li>

                                  <li> <a href="{{route('mypro.index')}}"><button type="button" class="btn btn-primary">  My Profile </button></a> </li>

                                  <li> <a href="{{route('mypass.index')}}"><button type="button" class="btn btn-primary"> Change Password </button></a> </li>

                                  
                               
                             </ul>     
                          </div>  
                          </nav> 
                      </div>           <!-- col-md-4 col-sm-4 -->
                   </div>

    @endsection

