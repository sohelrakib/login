@extends('master')


    @section('title')
      all product
    @endsection



    @section('content')  
 	  	         <h2 class="first-heading text-center">**** General Page * </h2>

	               <div class="row">
	               	           <p class="messsage text-center"> {{ session('message') }} </p>

      		                  <div class="col-md-8 col-sm-8">
                                         <div class="search">
                                    
                                    {!! Form::open(['route'=>'product.index','method'=>'GET','class'=>'search-form']) !!}
                                           <div class="form-group row">
                                               <div class="col-md-9 col-sm-9 col-xs-9">
                                                    <input type="text" name="name"  class="form-control col-md-10" placeholder="  search by name">
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    {!!Form::submit('search',array('class'=>'search-button btn btn-default')) !!}
                                                </div>
                                           </div>
                                    {!! Form:: close() !!}

                                         </div> <!--  search -->              
                         


                                        <div class="table-responsive">          
                                           <table class="table">
                 	                            <thead>
            									     <tr>
                                    <th> IMAGE </th>
            									     	<th> PRODUCT NAME </th> 
            									     	<th> PRICE </th>
            									     </tr>
                                                </thead>

                                                <tbody>
                                                      
            								     @foreach($alldata as $data)
            									      <tr>
                                       <td> 
                                         <img src="{{ asset('storage/products/'.$data->file_name)}}" alt="product" class="product-img img-responsive">
                                       </td>
            									         <td> {{$data->product_name}} </td>
            									         <td> {{$data->price}} </td>
            									      </tr>
            								    
            								     @endforeach 
                                                </tbody>
                                            </table>
                                        </div>   
                                        {!!$alldata->render() !!}                 
                             	  	         
      		                  </div>


      	               <div class="col-md-4 col-sm-4 sidebar ">
                                                      <nav class="navbar navbar-inverse ">
                                                       <div class="navbar-header">
                                                         <ul>
                                                           <li>
                                                        <button type="button" class="btn btn-success navbar-toggle sidebar-btn" data-toggle="collapse" data-target="#myNavbar2">
                                                             SIDEBAR
                                                                 
                                                          </button>
                                                        </li>
                                                      </ul>
                                                       </div>

                                                       <div class="collapse navbar-collapse" id="myNavbar2">
                          <ul class="sidebar-ul">
                             <li> <a href="{{route('login.create')}}"><button type="button" class="btn btn-primary"> Home </button></a> </li>

                             <!-- <li> <button type="button" class="btn btn-primary"><a href="{{route('product.create')}}"> Add New Product</a></button> </li> --> 

                              <li> <a href="{{route('product.index')}}"><button type="button" class="btn btn-primary"> All Product </button></a> </li>

                              <li> <a href="{{route('login.index')}}"><button type="button" class="btn btn-primary"> Our Member </button></a> </li> 

                              <li> <a href="{{route('order.index')}}"><button type="button" class="btn btn-primary"> Order a Product </button></a> </li>

                              <li> <a href="{{route('logout.index')}}"><button type="button" class="btn btn-primary"> Logout </button></a> </li>

                               <li> <a href="{{route('mypro.index')}}"><button type="button" class="btn btn-primary">  My Profile </button></a> </li>

                               <li> <a href="{{route('mypass.index')}}"><button type="button" class="btn btn-primary"> Change Password </button></a> </li>

                               
                            
                          </ul>     
                       </div>  
                       </nav> 
                   </div>           <!-- col-md-4 col-sm-4 -->

	               	           
			                  
	               </div>

    @endsection

 