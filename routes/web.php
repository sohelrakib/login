<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('regis','RegistrationController', ['middleware' => 'check']);

Route::resource('login','LoginController');

// Route::resource('product','ProductController', ['middleware' => 'checkLogout']);

// Route::resource('order','MyOrderController', ['middleware' => 'checkLogout']);

// Route::resource('logout','LogoutController', ['middleware' => 'checkLogout']);

// Route::resource('mypro','MyProfile', ['middleware' => 'checkLogout']);

// Route::resource('mypass','MyPassword', ['middleware' => 'checkLogout']);


Route::group(['middleware' => 'checkLogout'], function()
{
	Route::resource('product','ProductController');

    Route::resource('order','MyOrderController');

	Route::resource('logout','LogoutController');

	Route::resource('mypro','MyProfile');

    Route::resource('mypass','MyPassword');
});